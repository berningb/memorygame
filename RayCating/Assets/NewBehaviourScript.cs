﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour {
	void FixedUpdate()
    {
        if(Input.GetMouseButtonUp (0))
        {
            Vector3 mousePos = Input.mousePosition;

            RaycastHit hit;
            Ray myRay = Camera.main.ScreenPointToRay (mousePos);

            Debug.DrawRay(myRay.origin, myRay.direction, Color.magenta, 5.0f);

            if(Physics.Raycast (myRay, out hit))
            {
                GameObject thisIsHit = hit.transform.gameObject;
                thisIsHit.GetComponent<Rigidbody>().AddForceAtPosition(transform.forward * 1000, hit.point);
                thisIsHit.GetComponent<Renderer>().material.color = Color.red;
            }
        }
    }
}
