﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour {

    public static bool DO_NOT = false;

    [SerializeField]
    public int _state = 0;

    [SerializeField]
    public int _cardValue;

    [SerializeField]
    private bool _initialized;

    private Sprite _cardBack;
    private Sprite _cardFace;
    private GameObject _manager;
    private bool flipping = false;
    public int flipSide;

    void Start()
    {
        _manager = GameObject.FindGameObjectWithTag ("Manager");
    }
    void Update()
    {
        //print(_state);
        //if (flipping)
        //{
        //    transform.Rotate(Vector3.up * flipSide * 10);
        //    if (transform.rotation.eulerAngles.y >= 180)
        //    {
        //        flipping = false;
        //    }
        //    else if(transform.rotation.eulerAngles.y == 0) {
        //        flipping = false;
        //    }
        //}
    }

    public void setupGraphics ()
    {
        _cardBack = _manager.GetComponent<GameManager> ().getCardBack();
        _cardFace = _manager.GetComponent<GameManager> ().getCardFace(_cardValue);

        flipCard();
    }

    public void flipCard()
    {
        flipping = true;
        if (_state == 0)
        {
            flipSide = 1;
            _state = 1;
        }
        else if (_state == 1)
        {
            flipSide = -1;
            _state = 0;

        }

        if (_state == 0 && !DO_NOT)
            GetComponent<Image>().sprite = _cardBack;
        else if(_state == 1 && !DO_NOT)
            GetComponent<Image>().sprite = _cardFace;
    }

    public int cardValue
    {
        get { return _cardValue; }
        set { _cardValue = value; }
    }

    public int state
    {
        get { return _state; }
        set { _state = value; }
    }

    public bool initialized
    {
        get { return _initialized; }
        set { _initialized = value; }
    }

    public void falseCheck()
    {
        StartCoroutine ( pause());
    }

    IEnumerator pause ()
    {
        yield return new WaitForSeconds(1);
        if (_state == 0)
        {
            GetComponent <Image>().sprite = _cardBack;
        }
        else if (_state == 1)
        {
            GetComponent<Image>().sprite = _cardFace;
        }
        else if(_state == 2)
        {
            GetComponent<Image>().enabled = false;
        }
            DO_NOT = false;
    }

}
