﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalletSpawner : MonoBehaviour
{


    public int timeLeft = 0;
    public float offset;
    public float offset2;
    public float lastX;
    public float lastY;
    public GameObject player;
    public GameObject last;


    void Start()
    {
        offset = transform.position.x - player.transform.position.x;
        lastX = Random.Range(-8, 8);
        lastY = -4;
    }


    void Update()
    {
        timeLeft++;
        if (timeLeft % 60 == 0)
        {
            spawnPallet();
        }
    }
    void LateUpdate()
    {
        offset2 = player.GetComponent<Transform>().position.y + 7;
        transform.position = new Vector3(transform.position.x, player.transform.position.y + offset2, -10);

    }

    public void spawnPallet()
    {
        float widthAppart = Random.Range(3, 5);
        float heightAppart = Random.Range(3, 5);
        int randNum = Random.Range(-8, 8);
        float he;


        if (lastX > 0 && lastX < 4)
        {
            he = lastX + Random.Range(-1, 8);

        }
        else if (lastX < 4)
        {
            he = lastX + Random.Range(-4, 8);
        }
        else if (lastX < 0 && lastX > -4)
        {
            he = lastX + Random.Range(-8, 1);
        }
        else
        {
            he = lastX + Random.Range(-8, 4);
        }



        GameObject pallet = (GameObject)Instantiate(Resources.Load("Pallet"));
        pallet.GetComponent<Transform>().localScale = new Vector2(18, 2);


        print(he);
        pallet.GetComponent<Transform>().position = new Vector2(he, lastY + heightAppart);
        pallet.GetComponent<BoxCollider2D>().size = new Vector2(.14f, .14f);
        pallet.GetComponent<SpriteRenderer>().color = Color.gray;
        last = pallet;
        lastX = last.GetComponent<Transform>().position.x;
        lastY = last.GetComponent<Transform>().position.y;
    }

}