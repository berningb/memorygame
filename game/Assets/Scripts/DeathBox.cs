﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathBox : MonoBehaviour {

    public float offset;
    public float offset2;
    public GameObject player;

    void OnCollisionEnter2D(Collision2D col)
    {
        Destroy(col.gameObject);
    }

    void Start()
    {
        offset = transform.position.x - player.transform.position.x;
        GetComponent<BoxCollider2D>().offset = new Vector2(0, offset2);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = new Vector3(transform.position.x, player.transform.position.y + offset2 + 2, -10);
    }

}
