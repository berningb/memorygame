﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject player;
    private float offset;
    private float offset2;

	// Use this for initialization
	void Start () {
        offset = transform.position.x - player.transform.position.x;
        offset2 = transform.position.y - player.transform.position.y;
    }
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = new Vector3(transform.position.x, player.transform.position.y + offset2 + 2, -10);
	}
}
