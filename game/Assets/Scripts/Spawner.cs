﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public int timeLeft = 0;
    public float offset;
    public float offset2;
    public GameObject player;


    void Start()
    {
        offset = transform.position.x - player.transform.position.x;
    }

    void Update()
    {
        timeLeft++;
        if(timeLeft % 240 == 0)
        {
            spawnBlock();
        }
       
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = new Vector3(transform.position.x, player.transform.position.y + offset2, -10);
        offset2 = player.GetComponent<Transform>().position.y + 20;
    }

    public void spawnBlock()
    {
        float randHeight = Random.Range(15, 50);
        int randNum = Random.Range(-8, 8);

        GameObject block = (GameObject)Instantiate(Resources.Load("Block"));
        block.GetComponent<Transform>().localScale = new Vector2(randHeight, randHeight);
        block.GetComponent<Transform>().position = new Vector2(randNum, offset2);
        block.GetComponent<BoxCollider2D>().size = new Vector2(.14f, .14f);
        block.GetComponent<SpriteRenderer>().color = Color.red;
    }


    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "block")
        {
            Destroy(col.gameObject);
        }
    }
}
