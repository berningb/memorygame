﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseBehavior : MonoBehaviour {

    public Transform ball;
    public Rigidbody ballRigidBody;

    public List<GameObject> bridge;

void OnMouseUpAsButton()
    {
        Vector3 mousePos = Input.mousePosition;

        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(mousePos), out hit))
        {
            ball.position = hit.point;
            ballRigidBody.velocity = Vector3.zero;
        }
    }


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
