﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cube : MonoBehaviour {
    public GameObject ball;
    public Rigidbody rigi;
    public Cube otherL;
    public Cube otherR;
    public static bool leftStop = false;
    public static bool rightStop = false;

    void Start()
    {
        rigi.freezeRotation = true;
    }

    IEnumerator CubeFalling()
    {
        rigi.isKinematic = false;
        yield return new WaitForSeconds(.5f);

        if(otherL)
        {
            otherL.CallCoroutine();
        }
        else
        {
            leftStop = true;
        }
        if (otherR)
        {
            otherR.CallCoroutine();
        }
        else{
            rightStop = true;
        }
        if(leftStop && rightStop)
        {
            print("done");
            StopAllCoroutines();
            leftStop = false;
            rightStop = false;
            SceneManager.LoadScene(0);
        }


    }


    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag.Equals("ball") == true)
        {
            Debug.Log("Hit");
            StartCoroutine(CubeFalling());
        }
    }

    void CallCoroutine()
    {
        StartCoroutine(CubeFalling());
    }

}
