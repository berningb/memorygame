﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour {

    public GameObject ground;

    //Map height and mapWidth
    const int mapWidth = 50;
    const int mapHeight = 1;

    //Creating the array with width and height of a predefined value

    void Start()
    {
        //Create the map, and update the map
        int[,] MapArray = CreateMap();
        UpdateMap(MapArray);
    }

    //Define values where the 1's go here
    int[,] CreateMap()
    {
        int[,] mapArray = new int[mapWidth, mapHeight];

        mapArray[0, 0] = 1;
        mapArray[1, 0] = 1;
        mapArray[2, 0] = 1;
        mapArray[3, 0] = 1;

        return mapArray;
    }

    void UpdateMap(int[,] MapArray)
    {
        //Iterating through the list with 'x' and 'y' representing the x and y cartesian coordinates
        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                //If the current index has a value of 1
                if (MapArray[x, y] == 1)
                {
                    //Create a block at this point
                    Instantiate(ground, new Vector3(x, y, 0), Quaternion.identity);
                }
            }
        }
    }
}
